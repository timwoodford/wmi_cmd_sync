// Probably derived from GPL'd code for licensing purposes
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include "types.h"
#include "compiler.h"
#include "wmi.h"
#include "wil6210_mbc.h"
#define MBOX_BUFF_SIZE (240)

// By default, the WMI struct includes no space for channels, which 
// would cause memory corruption problems
struct start_cmd_with_channels {
    struct wmi_start_scan_cmd cmd;
    u16 chnl[1];
} __packed;

// Note: Might need to manually stop P2P scanning first
void send_scan_start(int fh) {
    struct wmi_cmd_hdr scan_cmd_hdr;
    scan_cmd_hdr.mid = MID_DEFAULT;
    scan_cmd_hdr.command_id = WMI_START_SCAN_CMDID;
    // The Python version sets this to 0.  Not sure if correct.
    scan_cmd_hdr.fw_timestamp = 0;
    struct start_cmd_with_channels scan_cmd;
    memset(&scan_cmd, 0, sizeof(scan_cmd));
    scan_cmd.cmd.discovery_mode=1;
    scan_cmd.cmd.scan_type=WMI_ACTIVE_SCAN;
    // Presumably, in our configuration, this will indicate how many times we 
    // cycle through the codebook entries, but I need to double-check.
    scan_cmd.cmd.num_channels=1;
    scan_cmd.cmd.channel_list[0].channel=0; // Shouldn't matter

    // If we do this inside the kernel, we would run:
    // wmi_send(wil, WMI_START_SCAN_CMDID, scan_cmd_hdr.mid, &cmd, sizeof(cmd));

    write(fh, &scan_cmd_hdr, sizeof(scan_cmd_hdr));
    write(fh, &scan_cmd, sizeof(scan_cmd));
}

void check_scan_start(int addr) {
    // off_virt=addr (this is the memory address to read)
    // reply_id=WMI_SCAN_COMPLETE_EVENTID
    unsigned char buf[MBOX_BUFF_SIZE];
    int len = 0;
    while(len==0) {
        len = __read_wmi(addr, WMI_SCAN_COMPLETE_EVENTID, buf, MBOX_BUFF_SIZE);
    }
}

u64 get_virtaddr(char *pcie_path) {
    void *baseaddr;
    return (u64) open_phyaddr_to_virtaddr(0, 2048000, &baseaddr, pcie_path);
}

int main() {
    // One option for receiving WMI mbox messages is to read from debugfs
    // However, apparently mmapping the relevant PCIe memory works better
    // Should be something like mbox
    //int fh_mb=open("/sys/debug/......", 0);
    // pcie_path should end in /resource_0
    u64 phyaddr = get_virtaddr("");
    // Should be something like wmi_send
    int fh_send=open("/sys/debug/......", 0);
    clock_t start=clock();
    send_scan_start(fh_send);
    // Check for response
    check_scan_start(phyaddr);
    clock_t end=clock();
    printf("ticks=%li, seconds per tick=%li\n", end-start, CLOCKS_PER_SEC);
}
