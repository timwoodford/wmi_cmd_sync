// Probably derived from GPL'd code for licensing purposes
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include "types.h"
#include "compiler.h"
#include "wmi.h"
#include "wil6210_mbc.h"
#define MBOX_BUFF_SIZE (240)


struct enter_tx_mode_cmd {
    u16 module_id;
    u16 ut_subtype_id;
    u32 enable;
    u8 rf_sector_id;
    u8 rf_gain_idx;
    u8 tx_bb_gain_row_num;
    u8 mcs;
    u8 reserved[88];
} __packed;

// Documented on page 149
// rf_gain range from 0 to 15
// rf_gain_row range from 0 to 31
void enter_tx(int fh, u8 rf_sector) {
    struct wmi_cmd_hdr scan_cmd_hdr;
    scan_cmd_hdr.mid = MID_DEFAULT;
    scan_cmd_hdr.command_id = WMI_UNIT_TEST_CMDID;
    // The Python version sets this to 0.  Not sure if correct.
    scan_cmd_hdr.fw_timestamp = 0;
    struct enter_tx_mode_cmd tx_cmd;
    memset(&tx_cmd, 0, sizeof(tx_cmd));
    tx_cmd.module_id=0xa;
    tx_cmd.ut_subtype_id=0xd;
    tx_cmd.enable=1;
    tx_cmd.rf_sector_id=rf_sector;
    tx_cmd.rf_gain_idx=1;
    tx_cmd.tx_bb_gain_row_num=1;
    tx_cmd.mcs=1;

    // If we do this inside the kernel, we would run:
    // wmi_send(wil, WMI_START_SCAN_CMDID, scan_cmd_hdr.mid, &cmd, sizeof(cmd));

    write(fh, &scan_cmd_hdr, sizeof(scan_cmd_hdr));
    write(fh, &tx_cmd, sizeof(tx_cmd));
}

void check_enter_tx(int addr) {
    // off_virt=addr (this is the memory address to read)
    // reply_id=WMI_SCAN_COMPLETE_EVENTID
    unsigned char buf[MBOX_BUFF_SIZE];
    int len = 0;
    while(len==0) {
        len = __read_wmi(addr, WMI_UNIT_TEST_EVENTID, buf, MBOX_BUFF_SIZE);
    }
}

u64 get_virtaddr(char *pcie_path) {
    void *baseaddr;
    return (u64) open_phyaddr_to_virtaddr(0, 2048000, &baseaddr, pcie_path);
}

int main(int argc, char *argv[]) {
    if(argc!=3) { printf("Usage\n"); return -1; }
    char *wmipath=argv[1];
    char *pcipath=argv[2];
    // pcie_path should end in /resource_0
    u64 phyaddr = get_virtaddr(pcipath);
    // Should be something like wmi_send
    int fh_send=open(wmipath, 0);
    clock_t start=clock();
    enter_tx(fh_send, 63);
    // Check for response
    check_enter_tx(phyaddr);
    clock_t end=clock();
    printf("ticks=%li, seconds per tick=%li\n", end-start, CLOCKS_PER_SEC);
}
