typedef unsigned char bool;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long u64;
typedef __signed__ char s8;
typedef __signed__ short s16;
typedef __signed__ int s32;


typedef u8			u_int8_t;
typedef u16			u_int16_t;
typedef u32			u_int32_t;
