// Pulled from the previous wil6210 manual control app code
// Removed Python interface
// Added errno.h

#include <stdio.h>      /* printf */
#include <stddef.h>     /* offsetof */
#include <string.h>     /* memset */
#include <ctype.h>      /* isascii */
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>

// #define MBC_DEBUG

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

#define max(x, y) (((x) > (y)) ? (x) : (y))
#define min(x, y) (((x) < (y)) ? (x) : (y))
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#define le16_to_cpus(x) do { (void)(x); } while (0)
#define le32_to_cpus(x) do { (void)(x); } while (0)
#define le16_to_cpu(val) (val)
#define le32_to_cpu(val) (val)

#define true 1
#define false 0

#define WIL6210_FW_HOST_OFF      (0x880000UL)
#define RGF_USER_USER_SCRATCH_PAD	(0x8802bc)
#define RGF_MBOX   RGF_USER_USER_SCRATCH_PAD
#define HOSTADDR(fwaddr)        (fwaddr - WIL6210_FW_HOST_OFF)
#define HOST_MBOX   HOSTADDR(RGF_MBOX)

#define MAX_MBOXITEM_SIZE   (240)

const char hex_asc[] = "0123456789abcdef";
#define hex_asc_lo(x)	hex_asc[((x) & 0x0f)]
#define hex_asc_hi(x)	hex_asc[((x) & 0xf0) >> 4]

#define PRINT_ERROR \
	do { \
		fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", \
		__LINE__, __FILE__, errno, strerror(errno)); exit(1); \
	} while(0)
	
typedef unsigned char bool;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long u64;
typedef unsigned char __le8;
typedef unsigned short __le16;
typedef unsigned int __le32;
typedef unsigned long __le64;
typedef int32_t s32;

enum {
	DUMP_PREFIX_NONE,
	DUMP_PREFIX_ADDRESS,
	DUMP_PREFIX_OFFSET
};

struct wil6210_mbox_ring {
	u32 base;
	u16 entry_size; /* max. size of mbox entry, incl. all headers */
	u16 size;
	u32 tail;
	u32 head;
};

struct wil6210_mbox_ring_desc {
	__le32 sync;
	__le32 addr;
};

struct wil6210_mbox_ctl {
	struct wil6210_mbox_ring tx;
	struct wil6210_mbox_ring rx;
};

struct wil6210_mbox_hdr {
	__le16 seq;
	__le16 len; /* payload, bytes after this header */
	__le16 type;
	u8 flags;
	u8 reserved;
};

struct wmi_cmd_hdr {
	u8 mid;
	u8 reserved;
	__le16 command_id;
	__le32 fw_timestamp;
};

/* Hardware definitions end */
struct fw_map {
	u32 from; /* linker address - from, inclusive */
	u32 to;   /* linker address - to, exclusive */
	u32 host; /* PCI/Host address - BAR0 + 0x880000 */
	const char *name; /* for debugfs */
	bool fw; /* true if FW mapping, false if UCODE mapping */
};

const struct fw_map fw_mapping[] = {
	/* FW code RAM 256k */
	{0x000000, 0x040000, 0x8c0000, "fw_code", true},
	/* FW data RAM 32k */
	{0x800000, 0x808000, 0x900000, "fw_data", true},
	/* periph data 128k */
	{0x840000, 0x860000, 0x908000, "fw_peri", true},
	/* various RGF 40k */
	{0x880000, 0x88a000, 0x880000, "rgf", true},
	/* AGC table   4k */
	{0x88a000, 0x88b000, 0x88a000, "AGC_tbl", true},
	/* Pcie_ext_rgf 4k */
	{0x88b000, 0x88c000, 0x88b000, "rgf_ext", true},
	/* mac_ext_rgf 512b */
	{0x88c000, 0x88c200, 0x88c000, "mac_rgf_ext", true},
	/* upper area 548k */
	{0x8c0000, 0x949000, 0x8c0000, "upper", true},
	/* UCODE areas - accessible by debugfs blobs but not by
	 * wmi_addr_remap. UCODE areas MUST be added AFTER FW areas!
	 */
	/* ucode code RAM 128k */
	{0x000000, 0x020000, 0x920000, "uc_code", false},
	/* ucode data RAM 16k */
	{0x800000, 0x804000, 0x940000, "uc_data", false},
};

/* Content of RF Sector (six 32-bits registers) */
struct wmi_rf_sector_info {
  /* Phase values for RF Chains[15-0] (2bits per RF chain) */
  __le32 psh_hi;
  /* Phase values for RF Chains[31-16] (2bits per RF chain) */
  __le32 psh_lo;
  /* ETYPE Bit0 for all RF chains[31-0] - bit0 of Edge amplifier gain
   * index
   */
  __le32 etype0;
  /* ETYPE Bit1 for all RF chains[31-0] - bit1 of Edge amplifier gain
   * index
   */
  __le32 etype1;
  /* ETYPE Bit2 for all RF chains[31-0] - bit2 of Edge amplifier gain
   * index
   */
  __le32 etype2;
  /* D-Type values (3bits each) for 8 Distribution amplifiers + X16
   * switch bits
   */
  __le32 dtype_swch_off;
} __packed;

struct module_level_enable { /* Little Endian */
	uint error_level_enable : 1;
	uint warn_level_enable : 1;
	uint info_level_enable : 1;
	uint verbose_level_enable : 1;
	uint reserved0 : 4;
} __attribute__((packed));

struct log_trace_header { /* Little Endian */
	/* the offset of the trace string in the strings sections */
	uint strring_offset : 20;
	uint module : 4; /* module that outputs the trace */
	/*	0 - Error
		1- WARN
		2 - INFO
		3 - VERBOSE */
	uint level : 2;
	uint parameters_num : 2; /* [0..3] */
	uint is_string : 1; /* indicate if the printf uses %s */
	uint signature : 3; /* should be 5 (2'101) in valid header */
} __attribute__((packed));

union log_event {
	struct log_trace_header hdr;
	u32 param;
} __attribute__((packed));

struct log_table_header {
	u32 write_ptr; /* incremented by trace producer every write */
	struct module_level_enable module_level_enable[16];
	union log_event evt[0];
} __attribute__((packed));

char *u32tobin(u32 a, char *buffer) {
    buffer[32] = '\0';
    buffer += 31;
    for (int i = 31; i >= 0; i--) {
        *buffer-- = (a & 1) + '0';
        a >>= 1;
    }
    return buffer;
}

int bin2int(const char *bin, int len) 
{
    int i = 0, j;
    j = sizeof(int)*8;
    while ( (j--) && ((*bin=='0') || (*bin=='1')) && len ) {
        i <<= 1;
        if ( *bin=='1' ) i++;
        bin++;
        len--;
    }
    return i;
}

char *valtobin(u32 a, char *buffer, u32 len) {
    buffer[len] = '\0';
    buffer += len-1;
    for (int i = len-1; i >= 0; i--) {
        *buffer-- = (a & 1) + '0';
        a >>= 1;
    }
    return buffer;
}

static u32 wmi_addr_remap(u32 x)
{
	uint i;

	for (i = 0; i < ARRAY_SIZE(fw_mapping); i++) {
		if (fw_mapping[i].fw &&
		    ((x >= fw_mapping[i].from) && (x < fw_mapping[i].to)))
			return x + fw_mapping[i].host - fw_mapping[i].from;
	}

	return 0;
}

void *wmi_buffer(__le32 ptr_)
{
	u64 off;
	u64 ptr = le32_to_cpu(ptr_);

	if (ptr % 4)
		return NULL;

	ptr = wmi_addr_remap(ptr);
	if (ptr < WIL6210_FW_HOST_OFF)
		return NULL;

	off = HOSTADDR(ptr);
	return (void *)off;
}

void *wmi_addr(u64 ptr)
{
	u64 off;

	if (ptr % 4)
		return NULL;

	if (ptr < WIL6210_FW_HOST_OFF)
		return NULL;

	off = HOSTADDR(ptr);
	return (void *)off;
}

void wil_mbox_ring_le2cpus(struct wil6210_mbox_ring *r)
{
	le32_to_cpus(&r->base);
	le16_to_cpus(&r->entry_size);
	le16_to_cpus(&r->size);
	le32_to_cpus(&r->tail);
	le32_to_cpus(&r->head);
}

void wil_memcpy_fromio_32(void *dst, const void *src,
			  size_t count)
{
	u32 *d = dst;
	const u32 *s = src;

	for (; count >= 4; count -= 4)
		*d++ = *(s++);

	if (unlikely(count)) {
		/* count can be 1..3 */
		u32 tmp = *(s);

		memcpy(d, &tmp, count);
	}
}

void wil_memcpy_toio_32(void *dst, const void *src,
			size_t count)
{
	u32 *d = dst;
	const u32 *s = src;

	for (; count >= 4; count -= 4)
		*(d++) = *(s++);

	if (unlikely(count)) {
		/* count can be 1..3 */
		u32 tmp = 0;

		memcpy(&tmp, s, count);
		*d = tmp;
	}
}

void hex_dump_to_buffer(const void *buf, size_t len, int rowsize,
			int groupsize, char *linebuf, size_t linebuflen,
			bool ascii)
{
	const u8 *ptr = buf;
	u8 ch;
	int j, lx = 0;
	int ascii_column;

	if (rowsize != 16 && rowsize != 32)
		rowsize = 16;

	if (!len)
		goto nil;
	if (len > rowsize)		/* limit to one line at a time */
		len = rowsize;
	if ((len % groupsize) != 0)	/* no mixed size output */
		groupsize = 1;

	switch (groupsize) {
	case 8: {
		const u64 *ptr8 = buf;
		int ngroups = len / groupsize;

		for (j = 0; j < ngroups; j++)
			lx += snprintf(linebuf + lx, linebuflen - lx,
					"%s%16.16llx", j ? " " : "",
					(unsigned long long)*(ptr8 + j));
		ascii_column = 17 * ngroups + 2;
		break;
	}

	case 4: {
		const u32 *ptr4 = buf;
		int ngroups = len / groupsize;

		for (j = 0; j < ngroups; j++)
			lx += snprintf(linebuf + lx, linebuflen - lx,
					"%s%8.8x", j ? " " : "", *(ptr4 + j));
		ascii_column = 9 * ngroups + 2;
		break;
	}

	case 2: {
		const u16 *ptr2 = buf;
		int ngroups = len / groupsize;

		for (j = 0; j < ngroups; j++)
			lx += snprintf(linebuf + lx, linebuflen - lx,
					"%s%4.4x", j ? " " : "", *(ptr2 + j));
		ascii_column = 5 * ngroups + 2;
		break;
	}

	default:
		for (j = 0; (j < len) && (lx + 3) <= linebuflen; j++) {
			ch = ptr[j];
			linebuf[lx++] = hex_asc_hi(ch);
			linebuf[lx++] = hex_asc_lo(ch);
			linebuf[lx++] = ' ';
		}
		if (j)
			lx--;

		ascii_column = 3 * rowsize + 2;
		break;
	}
	if (!ascii)
		goto nil;

	while (lx < (linebuflen - 1) && lx < (ascii_column - 1))
		linebuf[lx++] = ' ';
	for (j = 0; (j < len) && (lx + 2) < linebuflen; j++) {
		ch = ptr[j];
		linebuf[lx++] = (isascii(ch) && isprint(ch)) ? ch : '.';
	}
nil:
	linebuf[lx++] = '\0';
}

void print_hex_dump(const char *prefix_str, int prefix_type,
		    int rowsize, int groupsize,
		    const void *buf, size_t len, bool ascii)
{
	const u8 *ptr = buf;
	int i, linelen, remaining = len;
	unsigned char linebuf[32 * 3 + 2 + 32 + 1];

	if (rowsize != 16 && rowsize != 32)
		rowsize = 16;

	for (i = 0; i < len; i += rowsize) {
		linelen = min(remaining, rowsize);
		remaining -= rowsize;

		hex_dump_to_buffer(ptr + i, linelen, rowsize, groupsize,
				   (char*) linebuf, sizeof(linebuf), ascii);

		switch (prefix_type) {
		case DUMP_PREFIX_ADDRESS:
			printf("%s%p: %s\n",
			       prefix_str, ptr + i, linebuf);
			break;
		case DUMP_PREFIX_OFFSET:
			printf("%s%.8x: %s\n", prefix_str, i, linebuf);
			break;
		default:
			printf("%s%s\n", prefix_str, linebuf);
			break;
		}
	}
}

void wil_hexdump(void *p, int len, const char *prefix)
{
	print_hex_dump(prefix, DUMP_PREFIX_NONE, 16, 1, p, len, false);
}

u64 open_phyaddr_to_virtaddr(u64 mem_address, size_t mem_size, void **base_addr, const char *pcie_path) {
	int fd;
	void *map_base, *virt_addr;
	u64 target = mem_address;
	size_t MAP_SIZE = mem_size;
	size_t PAGE_SIZE = sysconf(_SC_PAGESIZE);
	size_t MAP_MASK = (PAGE_SIZE - 1);
	
	if((fd = open(pcie_path, O_RDWR | O_SYNC)) == -1) PRINT_ERROR;
    // printf("Target offset is 0x%x, page size is %ld\n", (int) target, sysconf(_SC_PAGE_SIZE));
    fflush(stdout);
    
    /* Map one page */
    // printf("mmap(%d, %ld, 0x%x, 0x%x, %d, 0x%x)\n", 0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (int) target);
    map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, target & ~MAP_MASK);
    if(map_base == (void *) -1) PRINT_ERROR;
    // printf("PCI Memory mapped to address 0x%08lx.\n", (unsigned long) map_base);
    fflush(stdout);
    close(fd);
	
	virt_addr = map_base + (target & MAP_MASK);
	*base_addr = map_base;
	printf("Open virtual memory address: %p\n", virt_addr);
	return (u64) virt_addr;
}

void close_phyaddr_to_virtaddr(void *map_base, size_t MAP_SIZE) {
	printf("Close virtual memory address: %p\n", map_base);
    if(munmap(map_base, MAP_SIZE) == -1) PRINT_ERROR;
}

void wil_print_ring(const u64 off_virt, const char *prefix)
{
    void *off = off_virt + HOST_MBOX + (void*) offsetof(struct wil6210_mbox_ctl, rx);
    
    struct wil6210_mbox_ring r;
	int rsize;
	uint i;
	
    wil_memcpy_fromio_32(&r, off, sizeof(r));
	wil_mbox_ring_le2cpus(&r);
	/*
	 * we just read memory block from NIC. This memory may be
	 * garbage. Check validity before using it.
	 */
	rsize = r.size / sizeof(struct wil6210_mbox_ring_desc);

	printf("ring %s = {\n", prefix);
	printf("  base = 0x%08x\n", r.base);
	printf("  size = 0x%04x bytes -> %d entries\n", r.size, rsize);
	printf("  tail = 0x%08x\n", r.tail);
	printf("  head = 0x%08x\n", r.head);
	printf("  entry size = %d\n", r.entry_size);
	
	if (r.size % sizeof(struct wil6210_mbox_ring_desc)) {
		printf("  ??? size is not multiple of %zd, garbage?\n",
			   sizeof(struct wil6210_mbox_ring_desc));
		goto out;
	}

	if (!wmi_addr(r.base) ||
	    !wmi_addr(r.tail) ||
	    !wmi_addr(r.head)) {
		printf("  ??? pointers are garbage?\n");
		goto out;
	}

	for (i = 0; i < rsize; i++) {
		struct wil6210_mbox_ring_desc d;
		struct wil6210_mbox_hdr hdr;
		size_t delta = i * sizeof(d);
		void *x = HOSTADDR((void *)(u64)r.base) + delta;

		// printf("r.base vir addr: %p\n", off_virt+(x-off));
		wil_memcpy_fromio_32(&d, x+off_virt, sizeof(d));

		printf("  [%2x] %s %s%s 0x%08x", i,
			   d.sync ? "F" : "E",
			   (r.tail - r.base == delta) ? "t" : " ",
			   (r.head - r.base == delta) ? "h" : " ",
			   le32_to_cpu(d.addr));


		void *src_d = wmi_buffer(d.addr);
		if (src_d && (u64)src_d != 0x40000) {
			wil_memcpy_fromio_32(&hdr, src_d + off_virt, sizeof(hdr));
			
			u16 len = le16_to_cpu(hdr.len);

			printf(" -> %04x %04x %04x %02x\n",
				   le16_to_cpu(hdr.seq), len,
				   le16_to_cpu(hdr.type), hdr.flags);
			if (len <= MAX_MBOXITEM_SIZE) {
				unsigned char databuf[MAX_MBOXITEM_SIZE];
				void *src_virt_d = src_d + off_virt + sizeof(struct wil6210_mbox_hdr);
				wil_memcpy_fromio_32(databuf, src_virt_d, len);
				wil_hexdump(databuf, len, "      : ");
			}
		} else {
			printf("\n");
		}
	}
 out:
	printf("}\n");
}

int __read_wmi(const u64 off_virt, u16 reply_id, void *databuf, u8 reply_size)
{
    void *off = off_virt + HOST_MBOX + (void*) offsetof(struct wil6210_mbox_ctl, rx);
    
    struct wil6210_mbox_ring r;
	int rsize;
	uint i;
	struct wmi_cmd_hdr *cmd_hdr;
	
    wil_memcpy_fromio_32(&r, off, sizeof(r));
	wil_mbox_ring_le2cpus(&r);
	/*
	 * we just read memory block from NIC. This memory may be
	 * garbage. Check validity before using it.
	 */
	rsize = r.size / sizeof(struct wil6210_mbox_ring_desc);
	
	if (r.size % sizeof(struct wil6210_mbox_ring_desc)) {
		printf("  ??? size is not multiple of %zd, garbage?\n",
			   sizeof(struct wil6210_mbox_ring_desc));
		return 0;
	}

	if (!wmi_addr(r.base) ||
	    !wmi_addr(r.tail) ||
	    !wmi_addr(r.head)) {
		printf("  ??? pointers are garbage?\n");
		return 0;
	}

	for (i = 0; i < rsize; i++) {
		struct wil6210_mbox_ring_desc d;
		struct wil6210_mbox_hdr hdr;
		size_t delta = i * sizeof(d);
		void *x = HOSTADDR((void *)(u64)r.base) + delta;
		wil_memcpy_fromio_32(&d, x+off_virt, sizeof(d));

		void *src_d = wmi_buffer(d.addr);
		if (src_d && (u64)src_d != 0x40000) {
			wil_memcpy_fromio_32(&hdr, src_d + off_virt, sizeof(hdr));
			
			u16 len = le16_to_cpu(hdr.len);

			if (len <= MAX_MBOXITEM_SIZE) {
				void *src_virt_d = src_d + off_virt + sizeof(struct wil6210_mbox_hdr);
				wil_memcpy_fromio_32(databuf, src_virt_d, len);
				cmd_hdr = (struct wmi_cmd_hdr *) databuf;
				if (cmd_hdr->command_id == reply_id) {
					return len;
				}
				if (cmd_hdr->command_id == 0xffff) {
					printf("NOT_SUPPORTED_EVENTID detected!!\n");
				}
			}
		}
	}
	
	return 0;
}

void mem_read_data(void *buf, const void * addr, u8 size) {
	u32 data = 0;
	u64 addr_aligned = ((u64) addr)/0x4*0x4;
	u8 byte_offset = ((u64) addr) - addr_aligned;
	void *p = (void *)&data;
	
	wil_memcpy_fromio_32(p, (void *) addr_aligned, 4);
	size = size > 4-byte_offset ? 4-byte_offset : size;
	memcpy(buf, (void *)p+byte_offset, size);
}

void __mem_dump(const void * addr, u32 size, unsigned char *databuf) {
	u64 addr_aligned = ((u64) addr)/0x4*0x4;
	
	#ifdef MBC_DEBUG
    printf("dump memory from: %p\n", (void *) addr_aligned);
    #endif
	wil_memcpy_fromio_32(databuf, (void *) addr_aligned, size);
	// wil_hexdump(databuf, size, "");
}

struct mem_block_write_data {
    u64 off_virt;
	u32 addr;
	u32 size;
	u32 data[];
};

static inline size_t log_size(size_t entry_num)
{
	return sizeof(struct log_table_header) + entry_num * 4;
}
