#ifndef _WIL6210_MBC_H_
#define _WIL6210_MBC_H_

extern u64 open_phyaddr_to_virtaddr(u64 mem_address, size_t mem_size, void **base_addr, const char *pcie_path);
extern void close_phyaddr_to_virtaddr(void *map_base, size_t MAP_SIZE);
extern int __read_wmi(const u64 off_virt, u16 reply_id, void *databuf, u8 reply_size);

#endif
