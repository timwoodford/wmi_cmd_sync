
all: run_scan sysapi_tx_start

run_scan: run_scan.c wil6210_mbc.c
	gcc -Wall -o $@ run_scan.c wil6210_mbc.c

sysapi_tx_start: sysapi_tx_start.c wil6210_mbc.c
	gcc -Wall -o $@ sysapi_tx_start.c wil6210_mbc.c
