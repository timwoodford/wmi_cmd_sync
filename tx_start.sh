sysfs_base=`find /sys/bus/pci/drivers/wil6210 -iname '00*' | head -n1`
sysfs_pcie=`find "$sysfs_base" -iname 'resource0'`
dbgfs_base=`find /sys/kernel/debug -iname 'wil6210' | head -n1`
dbgfs_wmi=`find "$dbgfs_base" -iname 'wmi_send' | head -n1`

./sysapi_tx_start "$dbgfs_wmi" "$sysfs_pcie"
